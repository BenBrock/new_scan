#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <mkl.h>

int *pscan(int *x, int *rv, int n, int z, int cs);

int main(int argc, char **argv)
{
  int i, j, l;
  int n, cs, best_cs;
  double begin, end, min;
  int *x, *seq, *rv;

  srand48(time(0));

  for (l = 2; l < 29; l++) {
    n = (int) pow(2, l);

    min = -1.0;

    for (cs = 2; cs < n; cs++) {

      if (n % cs == 0) {
        x = (int *) malloc(sizeof(int) * (n + cs));
        rv = (int *) malloc(sizeof(int) * (n + cs));
        seq = (int *) malloc(sizeof(int) * n);

        for (i = 0; i < n + cs; i++) {
          x[i] = 0;
          rv[i] = 0;
        }

        x += cs;
        rv += cs;

        for (i = 0; i < n; i++) {
          x[i] = lrand48() % 100 - 50;
          seq[i] = x[i];
        }

        for (i = 1; i < n; i++) {
          seq[i] += seq[i - 1];
        }

        begin = dsecnd();
        pscan(x, rv, n, 0, cs);
        end = dsecnd();

        if (min == -1.0 || end - begin < min) {
          min = end - begin;
          best_cs = cs;
        }

        for (i = 0; i < n; i++) {
          if (x[i] != seq[i]) {
            printf("Error, scan incorrect.\n");
            exit(0);
          }
        }

        // printf("Success with n: %d, cs: %d\n", n, cs);

        free(rv - cs);
        free(x - cs);
        free(seq);
      }
    }
    printf("%d %lf (%d)\n", n, min, best_cs);
    fflush(stdout);
  }

  return 0;
}

int *pscan(int *x, int *rv, int n, int z, int cs)
{
  int i, j, sum;

  #pragma omp parallel for
  for (i = 0; i < cs; i++) {
    for (j = i; j < n; j += cs) {
      rv[j] = x[j] + rv[j - cs];
    }
  }

  #pragma omp parallel for
  for (i = 0; i < n; i += cs) {
    sum = 0;
    for (j = i - cs; j < i; j++) {
      sum += rv[j];
    }

    for (j = i; j < i + cs; j++) {
      sum += x[j];
      x[j] = sum;
    }
  }

  return x;
}
