#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/scan.h>
#include <thrust/copy.h>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <ctime>

int main(int argc, char **argv)
{
  int n = atoi(argv[1]);
  float time;
  cudaEvent_t start, stop;

  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  thrust::host_vector <int> h_vec(n);
  std::generate(h_vec.begin(), h_vec.end(), rand);

  thrust::device_vector <int> d_vec = h_vec;

  cudaEventRecord(start, 0);
  thrust::inclusive_scan(d_vec.begin(), d_vec.end(), d_vec.begin());
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time, start, stop);
  time /= 1E3;

  thrust::copy(d_vec.begin(), d_vec.end(), h_vec.begin());

  printf("%d %f\n", n, time);

  return 0;
}
