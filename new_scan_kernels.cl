
#define reorder(i, cs, n) ((i) / (cs)) + ((i) % (cs)) * ((n) / (cs))

__kernel void pscan_one(__global int * restrict x, __global int * restrict rv, const int n, const int z, const int cs)
{
  int i, j, sum;

  rv += cs;
  x += cs;

  sum = 0;
  // j = get_global_id(0) * (n / cs);
  for (i = get_global_id(0); i < n; i += cs) {
    sum += x[i];
    x[i] = sum;
    // rv[j] = sum;
    // j++;
  }
}

__kernel void pscan_two(__global int * restrict x, __global int * restrict rv, const int n, const int z, const int cs)
{
  int i, j, sum;

  x += cs;
  rv += cs;

  sum = 0;

  if (get_global_id(0) != 0) {
    for (i = get_global_id(0) - 1; i < n; i += n / cs) {
      sum += rv[i];
    }
  }

  // j = get_global_id(0) * cs;
  for (i = get_global_id(0); i < n; i += n / cs) {
    sum += rv[i];
    if (get_global_id(0) != 0) {
      sum -= rv[i - 1];
    }
    //x[j] = sum;
    //j++;
    x[i] = sum;
  }
}

#define BLOCK_DIM 16
// interleave: global = bs x n / bs;  local = BLOCK_DIM x BLOCK_DIM
// uninterleave: global = n / bs x bs; local = BLOCK_DIM x BLOCK_DIM
kernel void transpose_local(__global int * restrict src,
                            __global int * restrict dest,
                            const int stride)
{
  src += stride;
  dest += stride;

  local int cache[(BLOCK_DIM + 1) * BLOCK_DIM];

  unsigned int tidx = get_global_id(0);
  unsigned int tidy = get_global_id(1);

  // fetch 
  // this will crash if the global, local and block sizes are not compatible
  unsigned int index_in = tidy * get_global_size(0) + tidx;
  cache[get_local_id(1) * (BLOCK_DIM + 1) + get_local_id(0)] = src[index_in];

  barrier(CLK_LOCAL_MEM_FENCE);

  // rotate and commit
  tidx = get_group_id(1) * BLOCK_DIM + get_local_id(0);
  tidy = get_group_id(0) * BLOCK_DIM + get_local_id(1);
  unsigned int index_out = tidy * get_global_size(1) + tidx;
  dest[index_out] = cache[get_local_id(0) * (BLOCK_DIM + 1) + get_local_id(1)];
}
