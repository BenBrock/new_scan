#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <CL/cl.h>
#include "clutils.h"
#include <chrono>
#include <iostream>
#include <cassert>

#define BLOCK_DIM 16

int main(int argc, char **argv)
{
  int i, j, l;
  int n, z, cs, best_cs;
  int zero;
  double min, min_k1, min_k2, min_int, min_int2;
  int *x, *seq, *rv;

  struct stat buf;
  FILE *f;
  char *k_source;

  zero = 0;

  /* OpenCL stuff. */
  cl_mem d_x, d_rv;
  cl_platform_id *platforms;
  cl_device_id device;
  cl_context context;
  cl_command_queue queue;
  cl_program program;
  cl_kernel pscan_one, pscan_two, transpose_local;
  cl_uint p_count;
  cl_int err;
  size_t gsize[2], lsize[2];
  size_t global_size, local_size;

  /* Initialization. */

  char *fname = "new_scan_kernels.cl";

  err = clGetPlatformIDs(0, NULL, &p_count);
  platforms = (cl_platform_id *) malloc(sizeof(cl_platform_id) * p_count);
  err = clGetPlatformIDs(p_count, platforms, NULL);
  clErrorString(err);
  err = clGetDeviceIDs(platforms[1], CL_DEVICE_TYPE_GPU, 1, &device, NULL);
  clErrorString(err);
  context = clCreateContext(0, 1, &device, NULL, NULL, &err);
  clErrorString(err);
  queue = clCreateCommandQueue(context, device, 0, &err);
  clErrorString(err);

  stat(fname, &buf);
  k_source = (char *) malloc(buf.st_size + 1);

  f = fopen(fname, "r");
  fread(k_source, buf.st_size, 1, f);
  k_source[buf.st_size] = '\0';

  program = clCreateProgramWithSource(context, 1, (const char **) &k_source, NULL, &err);
  clErrorString(err);

  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
  clErrorString(err);

  {
    char *build_log;
    size_t log_size;

    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

    if (log_size > 1) {
      printf("Build log:\n*******\n");

      build_log = (char *) malloc(sizeof(char) * (log_size + 1));

      clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size, build_log, NULL);
      build_log[log_size] = '\0';

      printf("%s", build_log);
      printf("*******\n");
      free(build_log);
    }
    if (err != CL_SUCCESS) {
      exit(1);
    }
  }

  pscan_one = clCreateKernel(program, "pscan_one", &err);
  pscan_two = clCreateKernel(program, "pscan_two", &err);
  transpose_local = clCreateKernel(program, "transpose_local", &err);

  srand48(0);

  z = 0;

  for (l = 10; l < 29; l++) {
    n = (int) pow(2, l);

    min = -1.0;

    for (cs = 256; cs < n; cs++) {

      if (n % cs == 0 && cs >= BLOCK_DIM && n / cs >= BLOCK_DIM) {
        seq = (int *) malloc(sizeof(int) * n);
        x = (int *) malloc(sizeof(int) * (n + cs));
        rv = (int *) malloc(sizeof(int) * (n + cs));

        d_rv = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(int) * (n + cs), NULL, NULL);
        d_x = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(int) * (n + cs), NULL, NULL);

        for (i = 0; i < n + cs; i++) {
          x[i] = 0;
        }

        clEnqueueWriteBuffer(queue, d_rv, CL_TRUE, 0, sizeof(int) * (n + cs), x, 0 , NULL, NULL);

        x += cs;
        rv += cs;

        for (i = 0; i < n; i++) {
          x[i] = lrand48() % 100 - 50;
          seq[i] = x[i];
        }

        for (i = 1; i < n; i++) {
          seq[i] += seq[i - 1];
        }

        clEnqueueWriteBuffer(queue, d_x, CL_TRUE, 0, sizeof(int) * (n + cs), x - cs, 0 , NULL, NULL);

        clSetKernelArg(pscan_one, 0, sizeof(cl_mem), &d_x);
        clSetKernelArg(pscan_one, 1, sizeof(cl_mem), &d_rv);
        clSetKernelArg(pscan_one, 2, sizeof(int), &n);
        clSetKernelArg(pscan_one, 3, sizeof(int), &z);
        clSetKernelArg(pscan_one, 4, sizeof(int), &cs);

        clSetKernelArg(transpose_local, 0, sizeof(cl_mem), &d_x);
        clSetKernelArg(transpose_local, 1, sizeof(cl_mem), &d_rv);
        clSetKernelArg(transpose_local, 2, sizeof(int), &cs);

        clSetKernelArg(pscan_two, 0, sizeof(cl_mem), &d_x);
        clSetKernelArg(pscan_two, 1, sizeof(cl_mem), &d_rv);
        clSetKernelArg(pscan_two, 2, sizeof(int), &n);
        clSetKernelArg(pscan_two, 3, sizeof(int), &z);
        clSetKernelArg(pscan_two, 4, sizeof(int), &cs);

        auto begin = std::chrono::high_resolution_clock::now();

        global_size = cs;
        clEnqueueNDRangeKernel(queue, pscan_one, 1, NULL, &global_size, NULL, 0, NULL, NULL);
        clFinish(queue);

        auto b_int = std::chrono::high_resolution_clock::now();

        gsize[0] = cs;
        gsize[1] = n / cs;
        lsize[0] = BLOCK_DIM;
        lsize[1] = BLOCK_DIM;
        clEnqueueNDRangeKernel(queue, transpose_local, 2, NULL, gsize, lsize, 0, NULL, NULL);
        clFinish(queue);

        auto a_int = std::chrono::high_resolution_clock::now();

        global_size = n / cs;
        clEnqueueNDRangeKernel(queue, pscan_two, 1, NULL, &global_size, NULL, 0, NULL, NULL);
        clFinish(queue);

        auto b_int2 = std::chrono::high_resolution_clock::now();

        gsize[0] = n / cs;
        gsize[1] = cs;
        lsize[0] = BLOCK_DIM;
        lsize[1] = BLOCK_DIM;
        clEnqueueNDRangeKernel(queue, transpose_local, 2, NULL, gsize, lsize, 0, NULL, NULL);
        clFinish(queue);
 
        auto end = std::chrono::high_resolution_clock::now();

        auto total = std::chrono::duration_cast <std::chrono::microseconds> (end - begin).count();
        auto kern1 = std::chrono::duration_cast <std::chrono::microseconds> (b_int - begin).count();
        auto kern2 = std::chrono::duration_cast <std::chrono::microseconds> (b_int2 - a_int).count();
        auto interleave1 = std::chrono::duration_cast <std::chrono::microseconds> (a_int - b_int).count();
        auto interleave2 = std::chrono::duration_cast <std::chrono::microseconds> (end - b_int2).count();

        clEnqueueReadBuffer(queue, d_rv, CL_TRUE, 0, sizeof(int) * (n + cs), rv - cs, 0, NULL, NULL);

        if (min == -1.0 || (double) total / 1E6 < min) {
          min = (double) total / 1E6;
          min_k1 = (double) kern1 / 1E6;
          min_k2 = (double) kern2 / 1E6;
          min_int = (double) interleave1 / 1E6;
          min_int2 = (double) interleave2 / 1E6;
          best_cs = cs;
        }

        for (i = 0; i < n; i++) {
          if (rv[i] != seq[i]) {
            printf("Error, scan incorrect.\n");
            //if (n < 128) {
              printf("For array:\n");
              for (i = 0; i < n; i++) {
                printf("%d ", x[i]);
              }
              printf("\n");
              printf("Got:\n");
              for (i = 0; i < n; i++) {
                printf("%d ", rv[i]);
              }
              printf("\n");
              printf("Should be:\n");
              for (i = 0; i < n; i++) {
                printf("%d ", seq[i]);
              }
              printf("\n");
            //}
            exit(0);
          }
        }

        // printf("Success with n: %d, cs: %d\n", n, cs);

        free(seq);
        free(rv - cs);
        free(x - cs);
        clReleaseMemObject(d_x);
        clReleaseMemObject(d_rv);
      }
    }
    printf("%d %lf (%d) 1: %lf int: %lf 2: %lf, int2: %lf\n", n, min, best_cs, min_k1, min_int, min_k2, min_int2);
    fflush(stdout);

  }

  free(platforms);
  clReleaseProgram(program);
  clReleaseKernel(pscan_one);
  clReleaseKernel(pscan_two);
  clReleaseKernel(transpose_local);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);

  return 0;
}
